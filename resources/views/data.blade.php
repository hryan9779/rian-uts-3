@extends('index')

@section('isi')
    <main id="main">
        <table border="5px" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Jenis</th>
                    <th>Habitat</th>
                    <th>Option</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($allitem as $item)
                    <tr align="center">
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->jenis }}</td>
                        <td>{{ $item->habitat }}</td>
                        <td>
                            <a href="{{ URL::to('data', $item->id) }}">Cek</a>
                            <a href="{{ URL::to('edit', $item->id) }}">Edit</a>
                            <a href="{{ URL::to('delete', $item->id) }}">Hapus</a>
                        </td>
                    </tr>
                @endforeach
                    <button><a href="buat">BUAT DATA</a></button>
                    <p>
                        <a href="/">Home</a>    

                    </p>
            </tbody>
        </table>
    </main>
@endsection