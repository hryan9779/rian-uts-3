@extends('index')

@section('isi')
    <main id="main">
        <form action="{{ url('update', $edit->id) }}" method="post">
            {{ csrf_field() }}
            <table border="5px" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <tr>
                    <th>Nama :</th>
                    <td>
                        <input type="text" name="nama" id="nama" value="{{ $edit->nama }}">
                    </td>
                </tr>
                <tr>
                    <th>Jurusan :</th>
                    <td>
                        <input type="text" name="jenis" id="jenis" value="{{ $edit->jenis }}">
                    </td>
                </tr>
                <tr>
                    <th>Habitat :</th>
                    <td>
                        <input type="text" name="habitat" id="habitat" value="{{ $edit->habitat }}">
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="/data">Kembali</a>
                    </td>
                    <td>
                        <button>EDIT</button>
                    </td>
                </tr>
            </table>
        </form>
    </main>
@endsection
