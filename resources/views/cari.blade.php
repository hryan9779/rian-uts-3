@extends('index')

@section('isi')
    <main id="main">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            @foreach ($cari as $item)
                <tr>
                    <th>ID : </th>
                    <td>{{ $item->id }}</td>
                </tr>
                <tr>
                    <th>Nama : </th>
                    <td>{{ $item->nama }}</td>
                </tr>
                <tr>
                    <th>Jenis : </th>
                    <td>{{ $item->jenis }}</td>
                </tr>
                <tr>
                    <th>Habitat : </th>
                    <td>{{ $item->habitat }}</td>
                </tr>
                <tr>
                    <th>Option</th>
                    <td>
                        <a href="/data">Kembali</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </main>
@endsection