<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Hewan;

class HewanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hewan::create([
            'nama' => 'Rusa',
            'jenis' => 'Herbivora',
            'habitat' => 'Hutan',
        ]);
        Hewan::create([
            'nama' => 'T-Rex',
            'jenis' => 'Karnivora',
            'habitat' => 'Hutan',
        ]);
        Hewan::create([
            'nama' => 'Ular',
            'jenis' => '???',
            'habitat' => 'Semak Semak',
        ]);

    }
}
