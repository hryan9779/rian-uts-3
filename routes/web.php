<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HewanController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('conten');
});

Route::get('data', [HewanController::class, 'index']);
Route::get('data/{id}', [HewanController::class, 'show']);
Route::get('buat', [HewanController::class, 'create']);
Route::post('simpan', [HewanController::class, 'store']);
Route::get('edit/{id}', [HewanController::class, 'edit']);
Route::post('update/{id}', [HewanController::class, 'update']);
Route::get('delete/{id}', [HewanController::class, 'destroy']);

